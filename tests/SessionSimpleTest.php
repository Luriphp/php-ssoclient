<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
namespace Luri\Sso {
	/** Mook std php function for test **/
	function setcookie($a, $b='', $c='', $d='', $e='', $f='', $g='') {
		if (!headers_sent()) {
			return \setcookie($a, $b, $c, $d, $e, $f, $g);
		} else {
			return true;
		}
	}

	function session_start($o=[]) {
		if (empty($_SESSION['PHPSESSID'])) {
			$_SESSION['PHPSESSID'] = rand();
		}
		return true;
	}

	function session_regenerate_id() {
		$_SESSION['PHPSESSID'] = rand();
		return true;
	}

	function session_id() {
		if (isset($_SESSION['PHPSESSID'])) {
			return $_SESSION['PHPSESSID'];
		} else {
			return "";
		}
	}

	function session_destroy() {
		$_SESSION = array();
	}

	function session_status() {
		if (!empty($_SESSION['PHPSESSID'])) {
			return PHP_SESSION_ACTIVE;
		} else {
			return PHP_SESSION_NONE;
		}
	}

}

namespace Luri\Sso\Test {
	use PHPUnit\Framework\TestCase;
	use Luri\Sso\SessionSimple;


	class SsoSimpleTest extends \PHPUnit\Framework\TestCase {
		/**
		 * Initialise SESSION VARIABLE
		 */
		protected function setUp(): void {
			$_SESSION = array();
		}

		public function testCreateSession() {
			$this->assertSame(PHP_SESSION_NONE, \Luri\Sso\session_status(), 'Une session est déjà active au début du test');

			$session = new \Luri\Sso\SessionSimple();

			$this->assertSame(PHP_SESSION_ACTIVE, \Luri\Sso\session_status(), 'Pas de session active');
			$this->assertNotEmpty($_SESSION['SS_DateRegenerate'], 'Pas de date pour le renouvellement de la session');
			$this->assertFalse(isset($_SESSION['SS_Expired']), 'Pourquoi une date d\'expiration est défini ?');
		}


		public function testDestroySession() {
			$session = new \Luri\Sso\SessionSimple();
			$this->assertNotEmpty($_SESSION, 'J\'ai besoin que session ssoit rempli pour ce test');
			$session->destroySession();

			$this->assertSame(PHP_SESSION_NONE, \Luri\Sso\session_status(), 'Mmmmm.... une session active');
			$this->assertEmpty($_SESSION, 'La session n\'est pas détruite');
		}


		public function testRegenerateSession() {
			$session = new \Luri\Sso\SessionSimple();
			$originalId = \Luri\Sso\session_id();
			$session['data'] = 'Ola';
			$session->regenerateSession();

			$this->assertSame(PHP_SESSION_ACTIVE, \Luri\Sso\session_status(), 'Pas de session active');
			$this->assertNotEmpty($_SESSION['SS_DateRegenerate'], 'Pas de date pour le renouvellement de la session');
			$this->assertFalse(isset($_SESSION['SS_Expired']), 'Pourquoi une date d\'expiration est défini ?');
			$this->assertNotSame($originalId, \Luri\Sso\session_id());
			$this->assertTrue(isset($session['data']), 'Les données utilisateurs ont disparu.');
			$this->assertSame('Ola', $session['data'] , 'Les données utilisateurs ont disparu.');
		}

		public function testRestartSession() {
			$session = new \Luri\Sso\SessionSimple();
			$originalId = \Luri\Sso\session_id();
			$session['data'] = 'Ola';
			$session->restartSession();

			$this->assertSame(PHP_SESSION_ACTIVE, \Luri\Sso\session_status(), 'Pas de session active');
			$this->assertNotEmpty($_SESSION['SS_DateRegenerate'], 'Pas de date pour le renouvellement de la session');
			$this->assertFalse(isset($_SESSION['SS_Expired']), 'Pourquoi une date d\'expiration est défini ?');
			$this->assertNotSame($originalId, \Luri\Sso\session_id());
			$this->assertFalse(isset($session['data']), 'Les données utilisateurs sont toujours présentes');
		}


		public function testArrayAccess() {
			$session = new \Luri\Sso\SessionSimple();

			$_SESSION['test'] = 'blablabla';
			$this->assertSame('blablabla', $session['test']);
			$session['ret'] = 'Aujour';
			$this->assertSame('Aujour', $_SESSION['ret']);
		}



		public function testSessionMustBeRenew() {
			$_SESSION['SS_DateRegenerate'] = time() - 601;
			$originalId = 2345678;
			$_SESSION['PHPSESSID']  = $originalId;

			$session = new \Luri\Sso\SessionSimple();

			$this->assertNotSame($originalId, \Luri\Sso\session_id());

		}


		public function testFreshNewExpiredSessionUse() {
			$_SESSION['SS_Expired'] = time() - 1;
			$originalId = 2345678;
			$_SESSION['PHPSESSID']  = $originalId;

			$session = new \Luri\Sso\SessionSimple();

			$this->assertSame($originalId, \Luri\Sso\session_id());
			$this->assertNotEmpty($_SESSION['SS_DateRegenerate'], 'Pas de date pour le renouvellement de la session');
		}


		public function testExpiredSessionUse() {
			$_SESSION['SS_Expired'] = time() - 6;
			$originalId = 2345678;
			$_SESSION['PHPSESSID']  = $originalId;

			$this->expectException(\Exception::class);
			$this->expectExceptionCode(2020);

			$session = new \Luri\Sso\SessionSimple();
		}



	}
}
?>