<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */

/**
 * CAS DE TEST :
 * Erreurs de param :
 * @todo Erreur url server
 * @todo Erreur secret
 * @todo Erreur id apps
 *
 * Joueur sans token : (anonyme)
 *  * 1ere visite page publique
 *  * visite page protege
 *
 * Joueur avec token (connecté) :
 *  * Page de retour après sso (ssoReturn)
 *  * 1ere page RL après retour sso (Echec validation de jeton)
 *  * 1ere page RL après retour sso (validation de jeton réussi)
 *  * page protégé token non expiré dernier chargement serveur = 20s
 *  * page protégé token non expiré  dernier chargement serveur = 61s (token valide serveur sso)
 *  * page protégé token non expiré  dernier chargement serveur = 61s (token invalide serveur sso)
 *  * page publique token non expiré  dernier chargement serveur = 20s
 *  * page publique token non expiré  dernier chargement serveur = 61s (token valide serveur sso)
 *  * page publique token non expiré  dernier chargement serveur = 61s (token invalide serveur sso)
 *  * page admin token non expiré  dernier chargement serveur = 1s
 *  * page admin token non expiré  dernier chargement serveur = 4s  (token valide serveur sso)
 *  * page admin token non expiré  dernier chargement serveur = 4s  (token invalide serveur sso)
 *  * page protégé token expiré depuis 10s (<graceTime)
 *  * page protégé token expiré depuis 20min (>graceTime)
 *  * page publique token expiré depuis 10s (<graceTime)
 *  * page publique token expiré depuis 20min (>graceTime)
 *  * Logout
 */

namespace {
	//tel to SsoClient where are in test mode
	define('PHPUNIT_TESTING', true);
}

namespace Luri\Sso\Test {
	use PHPUnit\Framework\TestCase;
	use Luri\Sso\SsoClient;
	use Psr\Http\Message\RequestInterface;
	use Psr\Http\Client\ClientInterface as HttpClient;
	use Http\Mock\Client as MockClient;
	use Http\Discovery\HttpClientDiscovery;
	use Http\Discovery\Strategy\MockClientStrategy;
	use GuzzleHttp\Psr7\Response;
	use Firebase\JWT\JWT;


class SsoClientTest extends TestCase {
	protected $token = 'FALSETOKEN4TEST';


	//Tel we are using a Mock client
	public function setUp(): void {
        HttpClientDiscovery::prependStrategy(MockClientStrategy::class);
	}

	/**
	 * Génère un client Mock avec comme réponse par défaut un code 204 (toekn invalide)
	 * @return MockClient
	 */
	protected function genMockClient() {
		$client = new MockClient();
		$jwt_default_response = JWT::encode([
			'code' => 204,
			'result' => 'Token is not valid',
		], 'SECRET', 'HS512');
		$client->setDefaultResponse(new Response('200', [], $jwt_default_response));
		return $client;
	}

	/**
	 * Ajoute comme réponse au $client : le visiteur Ronan (connecté / id 7)
	 *
	 * @param HttpClient $client
	 */
	protected function addRonanResponseToClient(HttpClient &$client) {
		$jwt_user_logged_response = JWT::encode([
			'code' => 200,
			'result' => [
				'anonymous' => false,
				'pseudo' => 'Ronan',
				'user' => 7,
				'level' => 0,
				'avatar' => 'http://localhost/fake/upload/Ronan.png'
			]
		], 'SECRET', 'HS512');
		$client->addResponse(new Response('200', [], $jwt_user_logged_response));
	}

	/**
	 * Vérifie que le client a bien demandé un nouveau jeton
	 * (utilisé plusieurs fois)
	 */
	protected function verifyAskedToken(SessionMock $session, $nbDisplayForm = 1) {
		$out = $this->getActualOutput();

		$this->assertStringStartsWith("<html>
	<body>
		<form ", $out);
		$this->assertEquals($nbDisplayForm, substr_count($out, 'http://localhost/fake/login.php'), 'URL not found');
		$this->assertEquals($nbDisplayForm, substr_count($out, 'name="app" value="ID"'), 'identification service not found');
		$this->assertEquals($nbDisplayForm, substr_count($out, 'name="command" value="attach"'), 'Demande de jeton non trouvé');

		$this->assertArrayNotHasKey('ssoToken', $session, 'Mmmm, le jeton ne devrait pas être en mémoire à ce stade');
		$this->assertArrayHasKey('ssoOriginPage', $session, 'Mmmm, on est sur une page protégé, donc une redirection devrait être lancé');
		$this->assertEquals('/request/uri/', $session['ssoOriginPage'], 'La sauvegarde de la page actuelle ne fonctionne pas');
	}

	protected function verifyCommandRequest(RequestInterface $request, $token, $command) {
		$this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
		$this->assertEquals('localhost', $request->getUri()->getHost());
		$this->assertEquals('/fake'.SsoClient::$urlAPICommand, $request->getUri()->getPath());
		$this->assertEquals('app=ID', $request->getUri()->getQuery());
		try {
			$params = (array) JWT::decode(substr($request->getBody(), 4), 'SECRET', array('HS512'));
			$this->assertEquals($command, $params['command']);
			$this->assertEquals($token, $params['param']->token);
		} catch (Exception $e) {
			$this->assertFalse(true, 'Erreur décode du JWT');

		}
	}


	/**
	 * Visiteur sans jeton
	 * 1ere visite page publique
	 * Ne devrait rien faire de particulier
	 */
	public function testFirstVisitPublicPage() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID');

		//Verify
		$out = $this->getActualOutput();
		$this->assertEmpty($out, "Mmmm, on est sur une page publique, donc aucune redirection ne devrait être lancé");

		$this->assertArrayNotHasKey('ssoOriginPage', $session, 'Mmmm, on est sur une page publique, donc aucune redirection ne devrait être lancé');
		$this->assertArrayNotHasKey('ssoToken', $session, 'Mmmm, le jeton ne devrait pas être en mémoire à ce stade');
	}

	/**
	 * 1ème visite d'une page protégé
	 * Devrait afficher une redirection POST vers le serveur SSO
	 */
	public function testFistVisitProtectedPage() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession();

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true);

		//Verify
		$this->verifyAskedToken($session);
	}

	/**
	 * Test de la fonction à appeller sur la page retour
	 *
	 * @runInSeparateProcess
	 */
	public function testSsoReturn() {
		//Set UP test
		$session = new SessionMock();
		$session->restartSession();
		$session['ssoOriginPage'] = '/request/uri/?variable=true';

		//Testing
		SsoClient::ssoReturn($session, 'TOKEN_SENDED');

		//Verify
		//Retreive header
		$headers = xdebug_get_headers();
		$location = null;
		foreach ($headers as $v) {
			if (substr($v, 0, 9)=='Location:') {
				$location = $v;
				break;
			}
		}
		$this->assertNotNull($location, 'We not find location header');
		$this->assertStringStartsWith('Location: /request/uri/?variable=true', $location);

		$this->assertArrayNotHasKey('ssoToken', $session, 'Mmmm, le jeton ne devrait pas être en mémoire à ce stade, dans la clé ssoToken');
		$this->assertArrayNotHasKey('ssoOriginPage', $session, 'La variable ssoOriginPage n\'a pas été effacé');
		$this->assertArrayHasKey('ssoReturn', $session, 'Mmmm, le jeton devrait être dans la clé ssoReturn');
		$this->assertEquals('TOKEN_SENDED', $session['ssoReturn'], 'Le jeton sauvegardé n\'est pas le bon.');
	}

	/**
	 * Après un retour du serveur, le client doit valider le jeton.
	 * Dans ce test, le jeton est valide
	 */
	public function testAfterAReturnWithValidToken() {
		//Set UP test
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoReturn'] = 'NEWTOKEN';

		$validTime = time() + 15*60;
		$client = $this->genMockClient();
		$jwt_CheckToken_response = JWT::encode([
			'code' => 200,
			'result' => [
				'validTime' => $validTime, //15min à partir de l'heure actuelle
				'graceTime' => 5*60 //5 min de plus
			]
		], 'SECRET', 'HS512');
		$client->addResponse(new Response('200', [], $jwt_CheckToken_response));
		$this->addRonanResponseToClient($client);


		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true, $client);


		//Verify
		//Pas de sortie HTML
		$out = $this->getActualOutput();
		$this->assertEmpty($out, "La sortie devrait être vide");

		//La requète envoyé est correct
		$request = $client->getRequests();
		$this->verifyCommandRequest($request[0], 'NEWTOKEN', 'CheckToken');
		$this->verifyCommandRequest($request[1], 'NEWTOKEN', 'GetUserInfo');

		//Les infos sont stockés correctement dans la session
			$this->assertArrayNotHasKey('ssoReturn', $session, 'La variable ssoReturn n\'a pas été effacé');
		$this->assertArrayHasKey('ssoToken', $session, 'Mmmm, le jeton devrait être dans la clé ssoToken');
		$this->assertEquals('NEWTOKEN', $session['ssoToken'], 'Valeur du jeton en session incorrect');
		$this->assertArrayHasKey('ssoTokenValidTime', $session, 'Mmmm, la variable ssoTokenValidTime devrait exister');
		$this->assertEquals($validTime, $session['ssoTokenValidTime'], 'limite de validité du jeton en session incorrect');
		$this->assertArrayHasKey('ssoTokenGraceTime', $session, 'Mmmm, la variable ssoTokenGraceTime devrait exister');
		$this->assertEquals(5*60, $session['ssoTokenGraceTime'], 'Temps de répit en session incorrect');

		//Verify interpretation of response
		$this->assertTrue($access->isLogged());
		$this->assertEquals(0, $access->getAdminLevel());
		$this->assertEquals(7, $access->getUserID());
		$this->assertEquals('Ronan', $access->getPseudo());
		$this->assertEquals('http://localhost/fake/upload/Ronan.png', $access->getAvatar());
	}


	/**
	 * Après un retour du serveur, le client doit valider le jeton.
	 * Dans ce test, le jeton est invalide, donc le clietn doit en demander un nouveau
	 */
	public function testAfterAReturnWithInvalidToken() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoReturn'] = 'FALSETOKEN';

		$client = $this->genMockClient();


		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true, $client);


		//Verify
		//sortie HTML (demande d'un nouveau token)
		$this->verifyAskedToken($session);

		//La requète envoyé est correcte
		$request = $client->getLastRequest();
		$this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
		$this->assertEquals('localhost', $request->getUri()->getHost());
		$this->assertEquals('/fake'.SsoClient::$urlAPICommand, $request->getUri()->getPath());
		$this->assertEquals('app=ID', $request->getUri()->getQuery());
		try {
			$params = (array) JWT::decode(substr($request->getBody(), 4), 'SECRET', array('HS512'));
			$this->assertEquals('CheckToken', $params['command']);
			$this->assertEquals('FALSETOKEN', $params['param']->token);
		} catch (Exception $e) {
			$this->assertFalse(true, 'Erreur décode du JWT');

		}

		//Les infos sont stockés correctement dans la session
		$this->assertArrayNotHasKey('ssoReturn', $session, 'La variable ssoReturn n\'a pas été effacé');
		$this->assertArrayNotHasKey('ssoTokenValidTime', $session, 'Mmmm, la variable ssoTokenValidTime ne devrait pas exister');
		$this->assertArrayNotHasKey('ssoTokenGraceTime', $session, 'Mmmm, la variable ssoTokenGraceTime ne devrait pas exister');

		//Vérifications des getters
		$this->assertFalse($access->isLogged());
		$this->assertEquals(-1, $access->getAdminLevel());
		$this->assertEquals('', $access->getPseudo());
		$this->assertEquals(0, $access->getUserID());
		$this->assertEmpty($access->getAvatar());
	}

	/**
	 * Joueur avec Token (connecté)
	 * page protégé token non expiré dernier chargement serveur = 20s
	 * Devrait prendre les données depuis la session
	 */
	public function testProtectedPageNotExpiredToken20SLast() {
		//Set UP test
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() + 10*60; //reste 10min
		$session['ssoTokenGraceTime'] = 60*5;
		$session['ssoUserInfoTime'] = time()-20;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];

		$client = $this->genMockClient(); //normally not used

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true, $client);

		//Verify
		//Pas de sortie HTML
		$out = $this->getActualOutput();
		$this->assertEmpty($out, "La sortie devrait être vide");

		//Pas de requète
		$this->assertFalse($client->getLastRequest(), 'Il ne devrait pas avoir de requète');

		//Vérifications des getters
		$this->assertTrue($access->isLogged());
		$this->assertEquals(0, $access->getAdminLevel());
		$this->assertEquals(7, $access->getUserID());
		$this->assertEquals('Ronan', $access->getPseudo());
		$this->assertEquals('http://localhost/fake/upload/Ronan.png', $access->getAvatar());
	}

	/**
	 * Joueur avec Token (connecté)
	 * page protégé token non expiré dernier chargement serveur = 61s et token valid serveur
	 * Devrait demander les données user au server
	 */
	public function testProtectedPageNotExpiredToken61SLastValidToken() {
		//Set UP test
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() + 10*60; //reste 10min
		$session['ssoTokenGraceTime'] = 60*5;
		$session['ssoUserInfoTime'] = time()-61;
		$session['ssoUserInfo'] = [ //False info here for detect incorrect set
			'anonymous' => true,
			'pseudo' => '',
			'user' => 0,
			'level' => -1,
			'avatar' => ''
		];

		$client = $this->genMockClient();
		$this->addRonanResponseToClient($client);

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true, $client);

		//Verify
		//Pas de sortie HTML
		$out = $this->getActualOutput();
		$this->assertEmpty($out, "La sortie devrait être vide");

		//Vérification de la requète
		$request = $client->getLastRequest();
		$this->verifyCommandRequest($request, 'TOKEN', 'GetUserInfo');

		//Vérifications des getters
		$this->assertTrue($access->isLogged());
		$this->assertEquals(0, $access->getAdminLevel());
		$this->assertEquals(7, $access->getUserID());
		$this->assertEquals('Ronan', $access->getPseudo());
		$this->assertEquals('http://localhost/fake/upload/Ronan.png', $access->getAvatar());
	}

	/**
	 * Joueur avec Token (connecté)
	 * page protégé token non expiré dernier chargement serveur = 61s et token invalid serveur
	 * Devrait demander les données user au server.
	 * Vu que le jeton est invalide, le client devrait en redemander un nouveau
	 */
	public function testProtectedPageNotExpiredToken61SLastInvalidToken() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() + 10*60; //reste 10min
		$session['ssoTokenGraceTime'] = 60*5;
		$session['ssoUserInfoTime'] = time()-61;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];;

		$client = $this->genMockClient();

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true, $client);

		//Verify
		//Vérification de la requète
		$request = $client->getLastRequest();
		$this->verifyCommandRequest($request, 'TOKEN', 'GetUserInfo');

		//Test radirection vers SSO
		$this->verifyAskedToken($session);

		//Vérifications des getters
		$this->assertFalse($access->isLogged());
		$this->assertEquals(-1, $access->getAdminLevel());
		$this->assertEmpty($access->getPseudo());
		$this->assertEmpty($access->getAvatar());
		$this->assertArrayNotHasKey('ssoUserInfo', $session);
		$this->assertArrayNotHasKey('ssoUserInfoTime', $session);
	}

	/**
	 * Joueur avec Token (connecté)
	 * page publique token non expiré dernier chargement serveur = 20s
	 * Devrait prendre les données depuis la session
	 */
	public function testPublicPageNotExpiredToken20SLast() {
		//Set UP test
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() + 10*60; //reste 10min
		$session['ssoTokenGraceTime'] = 60*5;
		$session['ssoUserInfoTime'] = time()-20;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];

		$client = $this->genMockClient(); //normally not used

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', false, $client);

		//Verify
		//Pas de sortie HTML
		$out = $this->getActualOutput();
		$this->assertEmpty($out, "La sortie devrait être vide");

		//Pas de requète
		$this->assertFalse($client->getLastRequest(), 'Il ne devrait pas avoir de requète');

		//Vérifications des getters
		$this->assertTrue($access->isLogged());
		$this->assertEquals(0, $access->getAdminLevel());
		$this->assertEquals(7, $access->getUserID());
		$this->assertEquals('Ronan', $access->getPseudo());
		$this->assertEquals('http://localhost/fake/upload/Ronan.png', $access->getAvatar());
	}

	/**
	 * Joueur avec Token (connecté)
	 * page protégé token non expiré dernier chargement serveur = 61s et token valid serveur
	 * Devrait demander les données user au server
	 */
	public function testPublicPageNotExpiredToken61SLastValidToken() {
		//Set UP test
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() + 10*60; //reste 10min
		$session['ssoTokenGraceTime'] = 60*5;
		$session['ssoUserInfoTime'] = time()-61;
		$session['ssoUserInfo'] = [ //False info here for detect incorrect set
			'anonymous' => true,
			'pseudo' => '',
			'user' => 0,
			'level' => -1,
			'avatar' => ''
		];

		$client = $this->genMockClient();
		$this->addRonanResponseToClient($client);

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', false, $client);

		//Verify
		//Pas de sortie HTML
		$out = $this->getActualOutput();
		$this->assertEmpty($out, "La sortie devrait être vide");

		//Vérification de la requète
		$request = $client->getLastRequest();
		$this->verifyCommandRequest($request, 'TOKEN', 'GetUserInfo');

		//Vérifications des getters
		$this->assertTrue($access->isLogged());
		$this->assertEquals(0, $access->getAdminLevel());
		$this->assertEquals(7, $access->getUserID());
		$this->assertEquals('Ronan', $access->getPseudo());
		$this->assertEquals('http://localhost/fake/upload/Ronan.png', $access->getAvatar());
	}

	/**
	 * Joueur avec Token (connecté)
	 * page publique token non expiré dernier chargement serveur = 61s et token invalid serveur
	 * Devrait demander les données user au server.
	 * Le jeton est invalide, les données de session devrait être supprimé mais
	 * aucune requète ne devrait être envoyé au serveur
	 */
	public function testPublicPageNotExpiredToken61SLastInvalidToken() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() + 10*60; //reste 10min
		$session['ssoTokenGraceTime'] = 60*5;
		$session['ssoUserInfoTime'] = time()-61;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];;

		$client = $this->genMockClient();

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', false, $client);

		//Verify
		//Vérification de la requète
		$request = $client->getLastRequest();
		$this->verifyCommandRequest($request, 'TOKEN', 'GetUserInfo');

		//Pas de sortie HTML
		$out = $this->getActualOutput();
		$this->assertEmpty($out, "La sortie devrait être vide");

		//Vérifications des getters
		$this->assertFalse($access->isLogged());
		$this->assertEquals(-1, $access->getAdminLevel());
		$this->assertEmpty($access->getPseudo());
		$this->assertEmpty($access->getAvatar());
		$this->assertEquals(0, $access->getUserID());
		$this->assertArrayNotHasKey('ssoUserInfo', $session);
		$this->assertArrayNotHasKey('ssoUserInfoTime', $session);
		$this->assertArrayNotHasKey('ssoToken', $session);
	}

	/**
	 * Joueur avec Token (connecté)
	 * page admin token non expiré dernier chargement serveur = 1s
	 * Devrait prendre les données depuis la session
	 */
	public function testAdminPageNotExpiredToken1SLast() {
		//Set UP test
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() + 10*60; //reste 10min
		$session['ssoTokenGraceTime'] = 60*5;
		$session['ssoUserInfoTime'] = time()-1;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];

		$client = $this->genMockClient(); //normally not used

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true, $client);
		$access->forceTokenVerif();

		//Verify
		//Pas de sortie HTML
		$out = $this->getActualOutput();
		$this->assertEmpty($out, "La sortie devrait être vide");

		//Pas de requète
		$this->assertFalse($client->getLastRequest(), 'Il ne devrait pas avoir de requète');

		//Vérifications des getters
		$this->assertTrue($access->isLogged());
		$this->assertEquals(0, $access->getAdminLevel());
		$this->assertEquals(7, $access->getUserID());
		$this->assertEquals('Ronan', $access->getPseudo());
		$this->assertEquals('http://localhost/fake/upload/Ronan.png', $access->getAvatar());
	}

	/**
	 * Joueur avec Token (connecté)
	 * page admin token non expiré dernier chargement serveur = 4s et token valid serveur
	 * Devrait demander les données user au server
	 */
	public function testAdminPageNotExpiredToken4SLastValidToken() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() + 10*60; //reste 10min
		$session['ssoTokenGraceTime'] = 60*5;
		$session['ssoUserInfoTime'] = time()-4;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];

		$client = $this->genMockClient();
		$this->addRonanResponseToClient($client);

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true, $client);
		$access->forceTokenVerif();


		//Verify
		//Vérification de la requète
		$request = $client->getLastRequest();
		$this->verifyCommandRequest($request, 'TOKEN', 'GetUserInfo');

		//Pas de sortie HTML
		$out = $this->getActualOutput();
		$this->assertEmpty($out, "La sortie devrait être vide");

		//Vérifications des getters
		$this->assertTrue($access->isLogged());
		$this->assertEquals(0, $access->getAdminLevel());
		$this->assertEquals(7, $access->getUserID());
		$this->assertEquals('Ronan', $access->getPseudo());
		$this->assertEquals('http://localhost/fake/upload/Ronan.png', $access->getAvatar());
	}

	/**
	 * Joueur avec Token (connecté)
	 * page admin token non expiré dernier chargement serveur = 4s et token invalid serveur
	 * Devrait demander les données user au server.
	 * Vu que le jeton est invalide, le client devrait en redemander un nouveau
	 */
	public function testAdminPageNotExpiredToken4SLastInvalidToken() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() + 10*60; //reste 10min
		$session['ssoTokenGraceTime'] = 60*5;
		$session['ssoUserInfoTime'] = time()-4;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];;

		$client = $this->genMockClient();

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true, $client);
		$access->forceTokenVerif();

		//Verify
		//Vérification de la requète
		$request = $client->getLastRequest();
		$this->verifyCommandRequest($request, 'TOKEN', 'GetUserInfo');

		//Test radirection vers SSO
		$this->verifyAskedToken($session);

		//Vérifications des getters
		$this->assertFalse($access->isLogged());
		$this->assertEquals(-1, $access->getAdminLevel());
		$this->assertEmpty($access->getPseudo());
		$this->assertEmpty($access->getAvatar());
		$this->assertEquals(0, $access->getUserID());
		$this->assertArrayNotHasKey('ssoUserInfo', $session);
		$this->assertArrayNotHasKey('ssoUserInfoTime', $session);
	}

	/**
	 * Joueur avec Token (connecté)
	 * page protégé token expiré depuis 10s (<graceTime)
	 * Devrait redemander un jeton
	 */
	public function testProtectedPageWithExpiredTokenButWeInGraceTime() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() - 10;
		$session['ssoTokenGraceTime'] = 60*5;
		$session['ssoUserInfoTime'] = time()-4;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];;

		$client = $this->genMockClient(); //normalement non  utilisé

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true, $client);

		//Verify
		//Pas de requète
		$this->assertFalse($client->getLastRequest(), 'Il ne devrait pas avoir de requète');
		//Test redirection vers SSO
		//Les conditions particulière du test unitaires font que dans ce cas précis, le formulaire de redirection est affiché deux fois. Ca ne sera pas le cas dans la réalité.
		$this->verifyAskedToken($session, 2);

		//Vérifications des getters
		$this->assertFalse($access->isLogged());
		$this->assertEquals(-1, $access->getAdminLevel());
		$this->assertEmpty($access->getPseudo());
		$this->assertEmpty($access->getAvatar());
		$this->assertArrayNotHasKey('ssoUserInfo', $session);
		$this->assertArrayNotHasKey('ssoUserInfoTime', $session);
	}

	/**
	 * Joueur avec Token (connecté)
	 * page protégé token expiré depuis 20min (>graceTime)
	 * Devrait redemander un jeton
	 */
	public function testProtectedPageWithExpiredTokenButWeNotInGraceTime() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() - 20*60;
		$session['ssoTokenGraceTime'] = 5*60;
		$session['ssoUserInfoTime'] = time()-4;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];;

		$client = $this->genMockClient(); //normalement non  utilisé

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true, $client);

		//Verify
		//Pas de requète
		$this->assertFalse($client->getLastRequest(), 'Il ne devrait pas avoir de requète');
		//Test radirection vers SSO
		$this->verifyAskedToken($session);

		//Vérifications des getters
		$this->assertFalse($access->isLogged());
		$this->assertEquals(-1, $access->getAdminLevel());
		$this->assertEmpty($access->getPseudo());
		$this->assertEmpty($access->getAvatar());
		$this->assertArrayNotHasKey('ssoUserInfo', $session);
		$this->assertArrayNotHasKey('ssoUserInfoTime', $session);
	}

	/**
	 * Joueur avec Token (connecté)
	 * page publique token expiré depuis 10s (<graceTime)
	 * Devrait redemander un jeton
	 */
	public function testPublicPageWithExpiredTokenButWeInGraceTime() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() - 10;
		$session['ssoTokenGraceTime'] = 60*5;
		$session['ssoUserInfoTime'] = time()-4;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];;

		$client = $this->genMockClient(); //normalement non  utilisé

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', false, $client);

		//Verify
		//Pas de requète
		$this->assertFalse($client->getLastRequest(), 'Il ne devrait pas avoir de requète');
		//Test redirection vers SSO
		$this->verifyAskedToken($session);

		//Vérifications des getters
		$this->assertFalse($access->isLogged());
		$this->assertEquals(-1, $access->getAdminLevel());
		$this->assertEmpty($access->getPseudo());
		$this->assertEmpty($access->getAvatar());
		$this->assertEquals(0, $access->getUserID());
		$this->assertArrayNotHasKey('ssoUserInfo', $session);
		$this->assertArrayNotHasKey('ssoUserInfoTime', $session);
	}

	/**
	 * Joueur avec Token (connecté)
	 * page public token expiré depuis 20min (>graceTime)
	 * Ne devrait pas redemander de jeton et devrait supprimer la session
	 */
	public function testPublicPageWithExpiredTokenButWeNotInGraceTime() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() - 20*60;
		$session['ssoTokenGraceTime'] = 5*60;
		$session['ssoUserInfoTime'] = time()-4;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];

		$client = $this->genMockClient(); //normalement non  utilisé

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', false, $client);

		//Verify
		//Pas de requète
		$this->assertFalse($client->getLastRequest(), 'Il ne devrait pas avoir de requète');
		//Pas de sortie HTML
		$out = $this->getActualOutput();
		$this->assertEmpty($out, "La sortie devrait être vide");

		//Vérifications des getters
		$this->assertFalse($access->isLogged());
		$this->assertEquals(-1, $access->getAdminLevel());
		$this->assertEmpty($access->getPseudo());
		$this->assertEmpty($access->getAvatar());
		$this->assertArrayNotHasKey('ssoUserInfo', $session);
		$this->assertArrayNotHasKey('ssoUserInfoTime', $session);
	}

	/**
	 * Test du loutout
	 * La lib doit effacer les données de sessions et rediriger vers le serveur
	 */
	public function testLogout() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() + 10 *60; //encore valide 10 min
		$session['ssoTokenGraceTime'] = 5*60;
		$session['ssoUserInfoTime'] = time()-4;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true);
		$access->logoutUser();

		//Verify
		$out = $this->getActualOutput();
		$this->assertStringStartsWith("<html>
	<body>
		<form ", $out);
		$this->assertEquals(1, substr_count($out, 'http://localhost/fake/login.php'), 'URL not found');
		$this->assertEquals(1, substr_count($out, 'name="action" value="logoff"'), 'Demande de déconnexion non trouvé');

		$this->assertArrayNotHasKey('ssoToken', $session);
		$this->assertArrayNotHasKey('ssoTokenValidTime', $session);
		$this->assertArrayNotHasKey('ssoTokenGraceTime', $session);
		$this->assertArrayNotHasKey('ssoUserInfoTime', $session);
		$this->assertArrayNotHasKey('ssoUserInfo', $session);
	}

	/**
	 * Test get some data from sso server
	 * La command et sa réponse sont une invention. C'est simplement pour tester
	 * qu'on peut facilement impélmenté des extension ua serveur sans changer la
	 * lib cliente.
	 *
	 */
	public function testgetDataFromserver() {
		//Set UP test
		$_SERVER['REQUEST_URI'] = '/request/uri/';
		$session = new SessionMock();
		$session->restartSession(); //Be sure Session is empty
		$session['ssoToken'] = 'TOKEN';
		$session['ssoTokenValidTime'] = time() + 10 *60; //encore valide 10 min
		$session['ssoTokenGraceTime'] = 5*60;
		$session['ssoUserInfoTime'] = time()-4;
		$session['ssoUserInfo'] = [
			'anonymous' => false,
			'pseudo' => 'Ronan',
			'user' => 7,
			'level' => 0,
			'avatar' => 'http://localhost/fake/upload/Ronan.png'
		];

		$client = $this->genMockClient(); //normalement non  utilisé
		$jwt_response = JWT::encode([ //Réponse inventé à ntore requète inventé
			'code' => 200,
			'result' => [
				'tags' => ['mj', 'holopolice']
			]
		], 'SECRET', 'HS512');
        $client->addResponse(new Response('200', [], $jwt_response));

		//Testing
		$access = new SsoClient($session, 'http://localhost/fake/', 'SECRET', 'ID', true, $client);
		//Send a personlisate request
		$user_ctags = $access->sendCommandToServer('getUserCTags', [
			'id' => [3, 10]
		]);

		//Verify request sended
		/* @var $request \Psr\Http\Message\RequestInterface  */
		$request = $client->getLastRequest();
		$this->assertInstanceOf('\Psr\Http\Message\RequestInterface', $request);
		$this->assertEquals('localhost', $request->getUri()->getHost());
		$this->assertEquals('/fake'.SsoClient::$urlAPICommand, $request->getUri()->getPath());
		$this->assertEquals('app=ID', $request->getUri()->getQuery());

		$params = (array) JWT::decode(substr($request->getBody(), 4), 'SECRET', array('HS512'));
		$this->assertEquals('getUserCTags', $params['command']);
		$this->assertEquals([3, 10], $params['param']->id);

		//Verify response returned
		$this->assertEquals(['mj', 'holopolice'], $user_ctags['result']['tags']);
	}
}
}
?>