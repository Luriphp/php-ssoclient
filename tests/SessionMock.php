<?php
/**
 *                                 SESSION SIMPLE
 *******************************************************************************
 * Class Wrapper arround php session for add a renew mecanism,
 * see https://www.php.net/manual/en/session.security.php
 *
 * (c) Luri <luri@e.email>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * sso seerver is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 * V1.1
 *
 */
namespace Luri\Sso\Test;

use Luri\Sso\SessionInterface;

/**
 * MOCK Class SessionInterface For phpunit test
 */
class SessionMock extends SessionInterface {
	/**
	 * Représent $_SESSION data for phpunit test
	 * @var mixed
	 */
	public $datas = [];

	public function __construct(\Psr\Log\LoggerInterface $log = null, $regenerateTime = 600, $expiredMaxTime = 5, $maxTimeLive = 1440) {
		$this->datas['PHPSESSID'] = rand();
	}

	/**
	 * Destroy Session and data associated
	 */
	public function destroySession() {
		$this->datas = [];
	}

	/**
	 * Destroy actual session and start a new one.
	 *
	 * USER DATA IS DELETED
	 */
	public function restartSession() {
		$this->datas = [];
		$this->datas['PHPSESSID'] = rand();
	}


	/**
	 * Regenerate the php session ID.
	 *
	 * USER DATA IS KEPT
	 */
	public function regenerateSession() {
		$this->datas['PHPSESSID'] = rand();
	}

	/*****************************
	 *   Array Access Interface  *
	 *****************************/
	public function offsetExists($offset) {
		return array_key_exists($offset, $this->datas);
	}
	public function offsetGet ($offset) {
		return $this->datas[$offset];
	}
	public function offsetSet ($offset, $value) {
		$this->datas[$offset] = $value;
	}
	public function offsetUnset($offset) {
		unset($this->datas[$offset]);
	}
}
?>