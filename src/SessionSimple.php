<?php
/**
 *                                 SESSION SIMPLE
 *******************************************************************************
 * Class Wrapper arround php session for add a renew mecanism,
 * see https://www.php.net/manual/en/session.security.php
 *
 * (c) Luri <luri@e.email>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * sso seerver is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 * V1.1-PHP5 : Some modif to be compatible with PHP5
 *
 */
namespace Luri\Sso;


/**
 * Wrapper Arround PHP SESSION mecanism
 *
 * WARNING : NOT USE SS_**** NAME FOR DATA
 *
 * simple replace $_SESSION par this object :
 * $session = new SessionSimple();
 * $session['ddd']=3;
 *
 */
class SessionSimple extends SessionInterface {
	/**
	 * Durée en secondes entre chaque regénération d'id
	 * @var int
	 */
	private $regenerateTime = 600;
	/**
	 * Durée de validité d'une session expiré (en secondes)
	 * @var int
	 */
	private $expiredMaxTime = 5;
	/**
	 * Durée en secondes de vie max d'une session si pas de rafraichissement
	 * @var int
	 */
	private $maxTimeLive = 1440;

	/**
	 * Logger
	 * @var \Psr\Log\LoggerInterface
	 */
	protected $logger = null;

	/**
	 * Start a session and verify her validity
	 *
	 * This USE this session var :
	 * $_SESSION['SS_Expired']
	 * $_SESSION['SS_DateRegenerate']
	 * NOT USE SAME VAR!!!
	 *
	 *
	 * @param int $minAccessLevel
	 * @param \Psr\Log\LoggerInterface $log, if you want log
	 * @param int $regenerateTime Durée en secondes entre chaque regénération d'id
	 * @param int $expiredMaxTime Durée de validité d'une session expiré (en secondes)
	 * @param int $maxTimeLive Durée en secondes de vie max d'une session si pas de rafraichissement
	 * @throws Exception code 2020 if use an expired session / code 412 if
	 */
	public function __construct(\Psr\Log\LoggerInterface $log = null, $regenerateTime = 600, $expiredMaxTime = 5, $maxTimeLive = 1440) {
		//Registered Logger
		if ($log instanceof \Psr\Log\LoggerInterface) {
			$this->logger = $log;
		} else {
			$this->logger = new \Psr\Log\NullLogger();
		}

		//Take user param
		if($regenerateTime > 0) {
			$this->regenerateTime = $regenerateTime;
		}
		if($expiredMaxTime > 0) {
			$this->expiredMaxTime = $expiredMaxTime;
		}
		if($maxTimeLive > 0) {
			$this->maxTimeLive = $maxTimeLive;
		}

		//Session start
		if (session_status() != PHP_SESSION_ACTIVE) {
			session_start();

		}

		//Save Generate Time
		if (empty($_SESSION['SS_DateRegenerate'])) {
			$_SESSION['SS_DateRegenerate'] = time();
		}

		//Delete VERY OLD SESSION
		if (($_SESSION['SS_DateRegenerate'] + $this->maxTimeLive) <= time()) {
			//An old session this must be not exist. Immediately destroy it
			$this->logger->alert('Use of a very old session. date: ' . date('d/m/Y H:i:s', $_SESSION['SS_DateRegenerate']));
			$this->restartSession();
		}

		//Delete Expired SESSION after a time
		if (!empty($_SESSION['SS_Expired']) AND ($_SESSION['SS_Expired'] + $this->expiredMaxTime) <= time()) {
			//after regenerate an id, some times is let before destroy because concurence access
			//Now, we delete this old session
			$this->logger->alert('Invalid session use. Expired: ' . date('d/m/Y H:i:s', $_SESSION['SS_Expired']));
			$this->destroySession();
			throw new \Exception('Session expired', 2020);
		}

		//Periodically regenerate session id for security
		if (($_SESSION['SS_DateRegenerate'] + $this->regenerateTime) <= time()) {
			$this->logger->debug('Regenerate session. date: ' . date('d/m/Y H:i:s', $_SESSION['SS_DateRegenerate']));
			$this->regenerateSession();
		}
	}

	/**
	 * Destroy Session and data associated
	 */
	public function destroySession() {
		//delete session variable
		$_SESSION = array();

		//Delete cookie
		if (ini_get("session.use_cookies") ) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}
		//Destroy session
		if (session_status() == PHP_SESSION_ACTIVE) {
			session_destroy();

		}
	}

	/**
	 * Destroy actual session and start a new one.
	 *
	 * USER DATA IS DELETED
	 */
	public function restartSession() {
		//Destroy actual session
		$this->destroySession();

		//And start a new one
		session_start();
		$_SESSION['SS_DateRegenerate'] = time();

	}

	/**
	 * Regenerate the php session ID.
	 *
	 * USER DATA IS KEPT
	 */
	public function regenerateSession() {
		//Invalidate existing session
		$_SESSION['SS_Expired'] = time();
		//regen
		session_regenerate_id();
		//revalidate session
		unset($_SESSION['SS_Expired']);
		//Save the date
		$_SESSION['SS_DateRegenerate'] = time();
	}

	/*****************************
	 *   Array Access Interface  *
	 *****************************/
	public function offsetExists($offset) {
		return array_key_exists($offset, $_SESSION);
	}
	public function offsetGet ($offset) {
		return $_SESSION[$offset];
	}
	public function offsetSet ($offset, $value) {
		$_SESSION[$offset] = $value;
	}
	public function offsetUnset($offset) {
		unset($_SESSION[$offset]);
	}
}
?>