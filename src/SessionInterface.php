<?php
/**
 *                                 SESSION SIMPLE
 *******************************************************************************
 * Class Wrapper arround php session for add a renew mecanism,
 * see https://www.php.net/manual/en/session.security.php
 *
 * (c) Luri <luri@e.email>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * sso seerver is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 * V1.1
 *
 */
namespace Luri\Sso;


/**
 * Wrapper Arround PHP SESSION mecanism
 *
 * WARNING : NOT USE SS_**** NAME FOR DATA
 *
 * Please use this abstract class for type. This can allow to use SessionMock For PHPUbitTest
 */
abstract class SessionInterface implements \ArrayAccess {
	/**
	 * Start a session and verify her validity
	 *
	 * This USE this session var :
	 * $_SESSION['SS_Expired']
	 * $_SESSION['SS_DateRegenerate']
	 * NOT USE SAME VAR!!!
	 *
	 *
	 * @param int $minAccessLevel
	 * @param \Psr\Log\LoggerInterface $log, if you want log
	 * @param int $regenerateTime Durée en secondes entre chaque regénération d'id
	 * @param int $expiredMaxTime Durée de validité d'une session expiré (en secondes)
	 * @param int $maxTimeLive Durée en secondes de vie max d'une session si pas de rafraichissement
	 * @throws Exception code 2020 if use an expired session
	 */
	public abstract function __construct(\Psr\Log\LoggerInterface $log = null, $regenerateTime = 600, $expiredMaxTime = 5, $maxTimeLive = 1440);

	/**
	 * Destroy Session and data associated
	 */
	public abstract function destroySession();

	/**
	 * Destroy actual session and start a new one.
	 *
	 * USER DATA IS DELETED
	 */
	public abstract function restartSession();


	/**
	 * Regenerate the php session ID.
	 *
	 * USER DATA IS KEPT
	 */
	public abstract function regenerateSession();
}
?>