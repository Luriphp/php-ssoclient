<?php
/**
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * Sso Client is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */

namespace Luri\Sso;

use Psr\Http\Client\ClientInterface;
use Http\Discovery\Psr18ClientDiscovery;
use Http\Message\MessageFactory;
use Http\Discovery\MessageFactoryDiscovery;
use \Firebase\JWT\JWT;
use Luri\Sso\Exception\HttpError;
use Luri\Sso\SessionInterface;

/**
 * SSoClient lib (compatbile PHP 5.6+)
 *
 * Use Session variable "sso*"
 *
 * You can access to any user info variable returned by ssoserver by user brace ($SsoClient['key'])
 */
class SsoClient implements \ArrayAccess, \IteratorAggregate {
	 /**
     * @var ClientInterface
     */
    protected $httpClient;
	/**
     * @var MessageFactory
     */
    protected $messageFactory;


	/**
	 * URL du server
	 *
	 * For example : https://sso.domain.fr/
	 *
	 * @var string
	 */
	protected $urlServer;
	/**
	 * Secret share with Server
	 *
	 * (use for generate session key)
	 * @var string
	 */
	protected $secret;
	/**
	 * Apps identify for server
	 * @var string
	 */
	protected $idenApps;
	/**
	 * True if user must be logged to access
	 * @var bool
	 */
	protected $mustBeLoggued;

	/**
	 * Relative url to login page in sso server
	 * @var string
	 */
	static $urlLogin = '/login.php';
	/**
	 * Relative url for get user info in sso server
	 * @var string
	 */
	static $urlAPICommand = '/command.php';

	/**
	 * user is logged or not
	 * indique si l'utiliseur est identifié ou pas
	 * @var boolean
	 */
	protected $isLogged = false;
	/**
	 * user Info
	 *
	 * Min response :
	 *   ['anonymous'] --> True (not loggedà / false (logged)
	 *   ['pseudo']
	 *   ['level'] --> level of user (-1 anonyme)
	 *   ['user'] --> user id
	 *
	 * @var array
	 */
	protected $userInfo = [];

	/**
	 * Simple Session handler for implement standard security advice
	 *
	 * Access Session variable with ArrayAccess ['xxx']
	 *
	 * Use it instead of $this->session.
	 *
	 * @var \Luri\Sso\SessionInterface
	 */
	protected $session;



	/**
	 * Start an SSO session
	 * we use PHP session and redirect function
	 *
	 * @param string $urlServer URL of identity server
	 * @param string $secret share secret with server
	 * @param string $idenApps Identity of this apps
	 * @param bool $mustBeLoggued Tel if user must be loggued or not (false by default)
	 * @param ClientInterface|null $httpClient Client to do HTTP requests, if not set, auto discovery will be used to find a HTTP client.
	 * @param MessageFactory|null $messageFactory to create PSR-7 requests.
	 */
	public function __construct(SessionInterface $session, $urlServer, $secret, $idenApps, $mustBeLoggued=false, ClientInterface $httpClient = null, MessageFactory $messageFactory = null) {
		$this->urlServer = $urlServer;
		$this->secret = $secret;
		$this->idenApps = $idenApps;
		$this->mustBeLoggued = (bool) $mustBeLoggued;

		//Start Session
		$this->session = $session;

		//Store or discovery httpClient and Message
		$this->httpClient = $httpClient ?: Psr18ClientDiscovery::find();
		$this->messageFactory = $messageFactory ?: MessageFactoryDiscovery::find();

		//After a SSoReturn, we must validate the token
		$validToken = $this->validateToken();

		if (!$validToken) {
			//We have an invalid Token
			$this->deleteToken(false);
			$this->session->restartSession();
		}

		//load Visitor Identify and verify Token
		$haveidentity = $this->loadIdentity();

		//On s'assure qu'il n'y ait pas des vieilles infos plus valide qui reste dans la session
		if (!$haveidentity AND isset($this->session['ssoToken'])) {
			$this->deleteToken(false);
		}

		//L'accès à une page protégé nécéssite d'en avoir le droit et donc d'être connecté.
		if ($this->mustBeLoggued && !$this->isLogged) {
			if ($haveidentity) {
				$this->deleteToken();
			}
			$this->AskToken();
		}

	}

	public function __destruct() {
		if (function_exists('sodium_memzero')) {
			sodium_memzero($this->secret);
			sodium_memzero($this->idenApps);
		}
	}

	/**
	 * If We are on Admin page, we must force a token Verification
	 * Force a token verification (if last verif > 2s)
	 *
	 * In PHPUNIT mode, this function return bool
	 * In normal mode, this function redirect to ssoserver if Token is not valid
	 */
	public function forceTokenVerif() {
		if (isset($this->session['ssoUserInfoTime']) AND ($this->session['ssoUserInfoTime'] + 2) < time()) {
			$res = $this->loadIdentifyFromServer();

			if (!$res) {
				$this->deleteToken(false);
				$this->askToken();
				return false;
			}
		}

		return true;
	}

	/**
	 * If we have a return from sserver, this function validate token and get important info (validTime, graceTime)
	 *
	 * @return boolean false if token is not valid
	 */
	protected function validateToken() {
		if (!isset($this->session['ssoReturn']) OR empty($this->session['ssoReturn'])) {
			return true;
		}

		//La page avant, c'était un retour sdu server SSO, il faut valider le token
		if (base64_decode($this->session['ssoReturn']) === false) {
			unset($this->session['ssoReturn']);
			return false;
		}

		$tokenInfo = $this->sendCommandToServer('CheckToken', ['token' => $this->session['ssoReturn']]);

		if ($tokenInfo['code'] == 200) {
			//Token valide
			$this->session['ssoToken'] = $this->session['ssoReturn'];
			unset($this->session['ssoReturn']);
			$this->session['ssoTokenValidTime'] = $tokenInfo['result']['validTime'];
			$this->session['ssoTokenGraceTime'] = $tokenInfo['result']['graceTime'];
			return true;
		} else {
			//Token invalide
			unset($this->session['ssoReturn']);
			return false;
		}
	}

	/**
	 * Load - if possible - Visitor Identity
	 *
	 * Return if Identity is loaded and secure or not
	 *
	 * @return bool true if we have a secure identity
	 */
	protected function loadIdentity() {
		if ($this->isHaveATokenAndHisNotExpired()) {
			//Token Non expiré, on charge les infos, soit depuis la session (dernière requète au serveur < 60s), soit depuis le serveur
			if (isset($this->session['ssoUserInfoTime']) AND ($this->session['ssoUserInfoTime']+60) > time()) {
				$this->loadIdentityFromSession();
				return true;
			} else {
				return $this->loadIdentifyFromServer();
			}

		} elseif ($this->isInGraceTime()) {
			//Token expiré mais on peut en redemander un autre, sans demande de reloggin
			$this->askToken();
			return false;

		} else {
			//Token expiré ou non existant
			return false;
		}
	}

	/**
	 * Indique si le visiteur a un token
	 * et s'il est toujours dans sa période de validité
	 * @return boolean
	 */
	protected function isHaveATokenAndHisNotExpired() {
		if (empty($this->session['ssoToken']) OR empty($this->session['ssoTokenValidTime']) OR empty($this->session['ssoTokenGraceTime'])) {
			return false;
		}

		return ($this->session['ssoTokenValidTime'] > time());
	}

	/**
	 * Est-ce qu'on est dans la période de répits (grace time)
	 * Il s'agit de la période pendant laquelle, l'utilisateur peut demander un nouveau jeton sans avoir à se relogguer.
	 * @return boolean
	 */
	protected function isInGraceTime() {
		//Si y'a pas les données, c'est qu'on est forcément pas dans la pérdiode de répit.
		if (!isset($this->session['ssoTokenValidTime']) OR !isset($this->session['ssoTokenGraceTime'])) {
			return false;
		}

		return (($this->session['ssoTokenValidTime'] + $this->session['ssoTokenGraceTime']) > time());

	}

	/**
	 * Delete token and inform server
	 *
	 * @param bool $deleteInServer False if Token is invalid over SSO server
	 */
	protected function deleteToken($deleteInServer = true) {

		//Delete token here
		$this->session['ssoToken']="";
		unset($this->session['ssoToken']);
		if (isset($this->session['ssoTokenValidTime'])) unset($this->session['ssoTokenValidTime']);
		if (isset($this->session['ssoTokenGraceTime'])) unset($this->session['ssoTokenGraceTime']);
		if (isset($this->session['ssoUserInfoTime'])) unset($this->session['ssoUserInfoTime']);
		if (isset($this->session['ssoUserInfo'])) unset($this->session['ssoUserInfo']);

		//Reset SsoClient Info
		$this->isLogged = false;
		$this->userInfo = [];

		//For security, Régen session
		$this->session->regenerateSession();

		//Say to server token is expired
		if($deleteInServer) {
			$this->sendCommandToServer('DeleteToken');
		}
	}

	/**
	 * Generate a new token and send it to server by client
	 *
	 * The public token is stocked into a session variable
	 *
	 * In Normal mode this function redirect to server
	 */
	protected function askToken() {
		//Save page where user is (for return)
		$this->session['ssoOriginPage'] = $_SERVER['REQUEST_URI'];

		//Param
		$param_app = htmlentities($this->idenApps);

		//URL and Query
		$url = self::urlConcat($this->urlServer, self::$urlLogin);

		//USE POST variable, so we must generate a form html and send them by javascript
		echo <<<VAR
<html>
	<body>
		<form id="ed" action="$url" method="post">
			<input type="hidden" name="command" value="attach" />
			<input type="hidden" name="app" value="$param_app" />
		</form>
		<p>Veuillez patientez, validation de l'acc&egrave;s en cour...</p>
		<script type="text/javascript">
			document.getElementById('ed').submit();
		</script>
	</body>
</html>
VAR;

		if(!defined('PHPUNIT_TESTING')) {
			exit();
		}
	}

	/**
	 * Manage Return after a attach or renew command to SSO server
	 *
	 * This function must be called in a return.php page or similary
	 *
	 * This function not include exit() (because unit test). You must add an exit() function after call this.
	 *
	 * @param string $token Token sended by SSOserver
	 */
	public static function ssoReturn(SessionInterface $session, $token) {
		if (empty($session['ssoOriginPage'])) {
			//Mmmm.... comment faire une redirection si on ne sait pas où aller?
			echo 'Erreur : Je ne sais pas d\'où vous venez désolé.';
			return;
		}

		// Set new token into special variable session.
		// Token is verified and registered into next page
		$session['ssoReturn'] = $token;

		//Redirection vers la page d'origine
		header('Location: ' . $session['ssoOriginPage']);
		echo '302 : Found';
		echo 'Redirect to origin page';

		//delete origin Page
		$session['ssoOriginPage'] = '';
		unset($session['ssoOriginPage']);
	}


	/**
	 * Send A Command to AuthServer and return response
	 *
	 * If token not exist (return code 204), we delete and regenerate actual token
	 *
	 * @param string $command
	 * @param array $param
	 * @return array JWT decoded into a array
	 * @throws Exception\InvalidArgumentException
	 * @throws Exception\HttpError if http code is different than 200
	 */
	public function sendCommandToServer($command, array $param = []) {
		//verify
		$command = strip_tags($command);
		if (!is_array($param)) {
			throw new Exception\InvalidArgumentException;
		}

		// Generate JWT
		$token = [
			'command' => $command,
			'param' => $param
		];
		$jwt = JWT::encode($token, $this->secret, 'HS512');

		//URL
		$url = self::urlConcat($this->urlServer, self::$urlAPICommand);
		//Add query param
		$url .= '?' . http_build_query([
			'app' => $this->idenApps
		]);

		//create and send request
		$request = $this->messageFactory->createRequest('POST', $url, ['Content-Type' => 'application/x-www-form-urlencoded']);
		$request->getBody()->write('jwt=' . $jwt);
		$response = $this->httpClient->sendRequest($request);

		//Verify response
		if ($response->getStatusCode() != 200) {
			//probablement une erreur
			throw new HttpError('Error ' . $response->getStatusCode(), $response->getStatusCode());
		}

		//reponse ok
		try {
			$jwtDecoded = (array) JWT::decode($response->getBody(), $this->secret, array('HS512'));
		} catch (\Exception $ex) {
			$this->session->destroySession();
			throw new Exception\SignatureError("Erreur de décodage du message du serveur. Vérifiez le paramétrage!");
		}
		if (isset($jwtDecoded['result'])) {
			$jwtDecoded['result'] = (Array) $jwtDecoded['result'];
		}

		return $jwtDecoded;
	}


	protected function loadIdentityFromSession() {
		//Some protect
		$ssoUserInfo = $this->session['ssoUserInfo'];
		$ssoUserInfo['pseudo'] = strip_tags($ssoUserInfo['pseudo']);
		$ssoUserInfo['level'] = (int) $ssoUserInfo['level'];
		$ssoUserInfo['user'] = (int) $ssoUserInfo['user'];

		//Load into class variable
		$this->userInfo = $ssoUserInfo;
		$this->isLogged = ! $this->userInfo['anonymous'];
	}

	/**
	 * Load client identify on server
	 * also, with load other information of client (mail...)
	 *
	 * @return bool true on success / false on error
	 */
	protected function loadIdentifyFromServer() {
		//Min Response :
		// ['anonymous'] --> True (not loggedà / false (logged)
		// ['pseudo']
		// ['level'] --> 0 user standard, >0:  admin level
		$userInfo = $this->sendCommandToServer('GetUserInfo', ['token' => $this->session['ssoToken']]);

		//Verify response
		if ($userInfo['code'] != 200) {
			return false;
		}

		//Protect default value
		$userInfo['result']['pseudo'] = strip_tags($userInfo['result']['pseudo']);
		$userInfo['result']['level'] = (int) $userInfo['result']['level'];
		if ($userInfo['result']['level'] < 0) $userInfo['result']['level'] = -1;
		$userInfo['result']['user'] = (int) $userInfo['result']['user'];
		if ($userInfo['result']['user'] < 0) $userInfo['result']['user'] = null;


		//Response ok, save this
		$this->userInfo = $userInfo['result'];
		$this->session['ssoUserInfo'] = $this->userInfo;
		$this->session['ssoUserInfoTime'] = time();

		//Is user is logged ?
		$this->isLogged = ! $this->userInfo['anonymous'];

		return true;
	}


	/**
	 * Logout a user
	 *
	 */
	public function logoutUser() {
		//Del token here
		$this->deleteToken(false);

		//URL and Query
		$url = self::urlConcat($this->urlServer, self::$urlLogin);

		//USE POST variable, so we must generate a form html and send them by javascript
		echo <<<VAR
<html>
	<body>
		<form id="ed" action="$url" method="post">
			<input type="hidden" name="action" value="logoff" />
		</form>
		<p>Veuillez patientez, la d&eacute;connexion est en cours...</p>
		<script type="text/javascript">
			document.getElementById('ed').submit();
		</script>
	</body>
</html>
VAR;

		if(!defined('PHPUNIT_TESTING')) {
			exit();
		}
	}


	/**
	 * Is the client is logged into server identity ?
	 *
	 * @return boolean
	 */
	public function isLogged() {
		return $this->isLogged;
	}

	/**
	 * Get client pseudo
	 *
	 * @return string
	 */
	public function getPseudo() {
		if (!empty($this->userInfo)) {
			return $this->userInfo['pseudo'];
		} else {
			//Defaut not loggued value
			return "";
		}
	}

	/**
	 * Get client avatar
	 *
	 * @return string
	 */
	public function getAvatar() {
		if (!empty($this->userInfo)) {
			return $this->userInfo['avatar'];
		} else {
			//Defaut not loggued value
			return "";
		}
	}

	/**
	 * Get admin level
	 * (0 = standard user)
	 * (-1 = Not Logged user)
	 *
	 * @return int
	 */
	public function getAdminLevel() {
		if (!empty($this->userInfo)) {
			return $this->userInfo['level'];
		} else {
			//Defaut not loggued value
			return -1;
		}
	}

	/**
	 * Return the user id
	 *
	 * 0 si l'utilisateur est annonyme
	 *
	 * @return int
	 */
	public function getUserID() {
		return (empty($this->userInfo)) ? 0 : $this->userInfo['user'];
	}

	/*****************************
	 *   Array Access Interface  *
	 *****************************/
	public function offsetExists($offset) {
		return array_key_exists($offset, $this->userInfo);
	}


	public function offsetGet ($offset) {
		return $this->userInfo[$offset];
	}
	public function offsetSet ($offset, $value) {
		throw new \BadFunctionCallException('SsoClient["key"] = "toto"; is not permited');
	}
	public function offsetUnset($offset) {
		throw new \BadFunctionCallException('Unset(SsoClient["key"]) is not permited');
	}

	/******************************************************
	 *   Return Iterator for use this class with foreach  *
	 ******************************************************/
	public function getIterator() {
		return new \ArrayIterator($this->userInfo);
	}

	/**********************
	 *   Helper function  *
	 **********************/
	/**
	 * Contact 2 part of 1 url with avoid no or two slash
	 *
	 * @param string $url1
	 * @param string $url2
	 */
	public static function urlConcat($url1, $url2) {
		if (substr($url1, -1)=='/' AND substr($url2, 0, 1)=='/') {
			//2 slash
			$url1 .= substr($url2, 1);
		} elseif (substr($url, -1)!='/' AND substr($url2, 0, 1)!='/') {
			//no slash
			$url1 .= '/' . $url2;
		} else {
			//normal
			$url1 .= $url2;
		}

		return $url1;
	}
}

?>